#include <stdint.h>

#include "st7735.h"
#include "ch32v003fun.h"
#include "lcd_hw.h"

static int spi_wait_tx() {
    uint32_t timeout = LCD_SPI_TX_TIMEOUT;

    while (SPI1->STATR & SPI_STATR_BSY) {
        if (--timeout == 0)
            return -1;
    }

    return 0;
}

extern void st7735_delay(int ms) {
    Delay_Ms(ms);
}

static int _st7735_write(uint16_t x, int cmd, int word) {
    if (spi_wait_tx())
        return -1;

    // 8 or 16 bit?
    if (!word)
        SPI1->CTLR1 &= ~SPI_CTLR1_DFF;
    else
        SPI1->CTLR1 |= SPI_CTLR1_DFF;

    // D/nC = 0 if cmd else 1.
    LCD_DC_GPIO->BSHR = 1 << ((cmd ? 16 : 0) + LCD_DC_PIN);

    SPI1->DATAR = x;
    return 0;
}


extern int st7735_write_cmd(uint8_t byte) {
    return _st7735_write(byte, 1, 0);
}

extern int st7735_write_data(uint8_t byte) {
    return _st7735_write(byte, 0, 0);
}

int lcd_display(int on) {
    return st7735_display(on);
}

int lcd_ramwr() {
    return st7735_ramwr();
}

int lcd_rect(int x, int y, int width, int height) {
    return st7735_rect(x, y, width, height);
}

int lcd_write_raw(uint16_t word) {
    return _st7735_write(word, 0, 1);
}

static void spi_config() {
    // Configure the SPI peripheral in its default master mode.
    // 8 data bits, baud rate of system clock / 2

    /* "Configure the NSS pin, for example by setting the SSOE bit and
        letting the hardware set the NSS. it is also possible to set the
        SSM bit and set the SSI bit high." */

    /* "To set the MSTR bit and the SPE bit, you need to make sure that
        the NSS is already high at this time." */
    SPI1->CTLR2 = SPI_CTLR2_SSOE | SPI_CTLR2_TXDMAEN;
    SPI1->CTLR1 = SPI_CTLR1_SPE | SPI_CTLR1_MSTR;
}

static int _lcd_dma(const void *buf, int x, int y, int width, int height, int repeat) {
    // Test alignment first.
    if ((uint32_t)buf & 1)
        return -1;

    uint32_t timeout = LCD_DMA_TIMEOUT;

    // Timeout waiting for the channel to become ready!
    while (DMA1_Channel3->CNTR) {
        if (--timeout == 0)
            return -1;
    }

    if (st7735_rect(x, y, width, height))
        return -1;

    if (st7735_ramwr())
        return -1;

    if (spi_wait_tx())
        return -1;

    // 16-bit mode
    SPI1->CTLR1 |= SPI_CTLR1_DFF;

    // D/nC = 1
    LCD_DC_GPIO->BSHR = 1 << LCD_DC_PIN;

    // XXX reset SPI, otherwise erroneous writes seem to occur?
    SPI1->CTLR1 &= ~SPI_CTLR1_SPE;

    // DMA1 channel 3 is wired to SPI TX.
    DMA1_Channel3->PADDR = (uint32_t)&SPI1->DATAR;
    DMA1_Channel3->MADDR = (uint32_t)buf;
    DMA1_Channel3->CNTR = width * height;
    DMA1_Channel3->CFGR = (repeat ? 0 : DMA_CFGR3_MINC) | DMA_CFGR3_DIR | DMA_Priority_VeryHigh | \
        DMA_MemoryDataSize_HalfWord | DMA_PeripheralDataSize_HalfWord;
    DMA1_Channel3->CFGR |= DMA_CFGR3_EN;

    // XXX re-enable SPI
    SPI1->CTLR1 |= SPI_CTLR1_SPE;

    return 0;
}

int lcd_draw(const void *buf, int x, int y, int width, int height) {
    return _lcd_dma(buf, x, y, width, height, 0);
}

int lcd_fill_rect(int x, int y, int width, int height, uint16_t color) {
    uint16_t val __attribute__((aligned(2))) = color;
    return _lcd_dma(&val, x, y, width, height, 1);
}


#define CFGLR_SET(gpio, pin, setting) \
    gpio->CFGLR &= ~(0xf << (4 * (pin))); \
    gpio->CFGLR |= (setting) << (4 * (pin));

int lcd_hw_init() {
    RCC->APB2PCENR |= RCC_APB2Periph_GPIOC | RCC_APB2Periph_SPI1 | LCD_DC_GPIO_APB2_EN_BIT;
    RCC->AHBPCENR |= RCC_AHBPeriph_DMA1;

    // MOSI
    CFGLR_SET(GPIOC, 6, GPIO_Speed_50MHz | GPIO_CNF_OUT_PP_AF);

    // SCK
    CFGLR_SET(GPIOC, 5, GPIO_Speed_50MHz | GPIO_CNF_OUT_PP_AF);

    // NSS
    CFGLR_SET(GPIOC, 1, GPIO_Speed_50MHz | GPIO_CNF_OUT_PP_AF);

    // D/C
    CFGLR_SET(LCD_DC_GPIO, LCD_DC_PIN, GPIO_Speed_50MHz | GPIO_CNF_OUT_PP);

    spi_config();

    return st7735_init();
}
