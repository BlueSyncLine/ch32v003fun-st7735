#ifndef LCD_DRAW_H
#define LCD_DRAW_H

#define LCD_RGB565(r, g, b) ((int)(r) << 11 | (int)(g) << 5 | (int)(b))
#include "lcd_hw.h"
int lcd_draw_char(int x, int y, uint16_t bg, uint16_t fg, char c);
int lcd_draw_string(int x, int y, uint16_t bg, uint16_t fg, char *s);
#endif
