#include "st7735.h"
#include <stdint.h>

// The power-on state of the display is outlined in: 9.14 - Reset Table.
static int st7735_cmd_with_delay(uint8_t cmd) {
    if (st7735_write_cmd(cmd))
        return -1;

    st7735_delay(120);
    return 0;
}

static int st7735_write_cmd_data(uint8_t cmd, uint8_t data) {
    if (st7735_write_cmd(cmd))
        return -1;

    if (st7735_write_data(data))
        return -1;

    return 0;
}

// 10.1.2 SW reset
int st7735_sw_reset() {
    return st7735_cmd_with_delay(ST7735_CMD_SWRESET);
}

// 10.1.10/11 Sleep In/Out
int st7735_sleep(int sleep) {
    return st7735_cmd_with_delay(sleep ? ST7735_CMD_SLPIN : ST7735_CMD_SLPOUT);
}

// 10.1.17/18 Display Off/On
int st7735_display(int on) {
    return st7735_cmd_with_delay(on ? ST7735_CMD_DISPON : ST7735_CMD_DISPOFF);
}

// 10.1.19/20 CASET and RASET
int st7735_rect(int x, int y, int w, int h) {
    if (st7735_write_cmd(ST7735_CMD_CASET))
        return -1;
    if (st7735_write_data(0))
        return -1;
    if (st7735_write_data(x))
        return -1;
    if (st7735_write_data(0))
        return -1;
    if (st7735_write_data(x + w - 1))
        return -1;

    if (st7735_write_cmd(ST7735_CMD_RASET))
        return -1;
    if (st7735_write_data(0))
        return -1;
    if (st7735_write_data(y))
        return -1;
    if (st7735_write_data(0))
        return -1;
    if (st7735_write_data(y + h - 1))
        return -1;

    return 0;
}

int st7735_ramwr() {
    return st7735_write_cmd(ST7735_CMD_RAMWR);
}

int st7735_init() {
    if (st7735_sw_reset())
        return -1;

    if (st7735_sleep(0))
        return -1;

    // Horizontal mode.
    if (st7735_write_cmd_data(ST7735_CMD_MADCTL, ST7735_MADCTL_MY | ST7735_MADCTL_MV))
        return -1;

    if (st7735_write_cmd_data(ST7735_CMD_COLMOD, ST7735_COLMOD_16BPP))
        return -1;

    return 0;
}
