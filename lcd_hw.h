#ifndef LCD_HW_H
#define LCD_HW_H

#include <stdint.h>
#include "ch32v003fun.h"

#define LCD_SPI_TX_TIMEOUT 1000000
#define LCD_DMA_TIMEOUT    1000000

#define LCD_DC_GPIO GPIOC
#define LCD_DC_PIN 0
#define LCD_DC_GPIO_APB2_EN_BIT RCC_APB2Periph_GPIOC

#define LCD_WIDTH 160
#define LCD_HEIGHT 128

int lcd_hw_init();
int lcd_display(int on);
int lcd_rect(int x, int y, int width, int height);
int lcd_ramwr();
int lcd_write_raw(uint16_t word);
int lcd_draw(const void *buf, int x, int y, int width, int height);
int lcd_fill_rect(int x, int y, int width, int height, uint16_t color);
#endif
