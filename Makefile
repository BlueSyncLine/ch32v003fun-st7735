PREFIX ?= riscv32-unknown-elf
TARGET := prog
ADDITIONAL_C_FILES+=st7735.c lcd_hw.c lcd_draw.c font.c img.c

all: $(TARGET).bin
include ../../ch32v003fun/ch32v003fun.mk

clean: cv_clean

flash: all
	../../minichlink/minichlink -c /dev/ttyACM0 -w $(TARGET).bin flash -T
