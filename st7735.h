#ifndef ST7735_H
#define ST7735_H

#include <stdint.h>

#define ST7735_WIDTH 160
#define ST7735_HEIGHT 128

#define ST7735_CMD_NOP          0x00
#define ST7735_CMD_SWRESET      0x01
#define ST7735_CMD_RDDID        0x04
#define ST7735_CMD_RDDST        0x09
#define ST7735_CMD_RDDPM        0x0a
#define ST7735_CMD_RDDMADCTL    0x0b
#define ST7735_CMD_RDDCOLMOD    0x0c
#define ST7735_CMD_RDDIM        0x0d
#define ST7735_CMD_RDDSM        0x0e
#define ST7735_CMD_SLPIN        0x10
#define ST7735_CMD_SLPOUT       0x11
#define ST7735_CMD_PTLON        0x12
#define ST7735_CMD_NORON        0x13
#define ST7735_CMD_INVOFF       0x20
#define ST7735_CMD_INVON        0x21
#define ST7735_CMD_GAMSET       0x26
#define ST7735_CMD_DISPOFF      0x28
#define ST7735_CMD_DISPON       0x29
#define ST7735_CMD_CASET        0x2a
#define ST7735_CMD_RASET        0x2b
#define ST7735_CMD_RAMWR        0x2c
#define ST7735_CMD_RAMRD        0x2e
#define ST7735_CMD_PTLAR        0x30
#define ST7735_CMD_TEOFF        0x34
#define ST7735_CMD_TEON         0x35
#define ST7735_CMD_MADCTL       0x36
#define ST7735_CMD_IDMOFF       0x38
#define ST7735_CMD_IDMON        0x39
#define ST7735_CMD_COLMOD       0x3a
#define ST7735_CMD_RDID1        0xda
#define ST7735_CMD_RDID2        0xdb
#define ST7735_CMD_RDID3        0xdc

#define ST7735_MADCTL_MH        (1<<2)
#define ST7735_MADCTL_RGB       (1<<3)
#define ST7735_MADCTL_ML        (1<<4)
#define ST7735_MADCTL_MV        (1<<5)
#define ST7735_MADCTL_MX        (1<<6)
#define ST7735_MADCTL_MY        (1<<7)

#define ST7735_COLMOD_12BPP     3
#define ST7735_COLMOD_16BPP     5
#define ST7735_COLMOD_18BPP     6

extern void st7735_delay(int ms);
extern int st7735_write_cmd(uint8_t byte);
extern int st7735_write_data(uint8_t byte);

int st7735_sw_reset();
int st7735_sleep(int sleep);
int st7735_display(int on);
int st7735_rect(int x, int y, int w, int h);
int st7735_ramwr();
int st7735_init();
#endif
