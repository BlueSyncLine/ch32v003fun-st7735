ch32v003fun with a ST7735 LCD
=============================

This the product of me playing around with a ST7735 display and a CH32V003 board.

Functions to initialize the display and render text are provided, along with DMA-based blitting.

The hard-coded settings assume a 128x160 LCD in horizontal orientation.

Connections
===========
The display module is expected to have a 4-wire SPI interface (MOSI, D/C, SCK, CS).
The connections are as follows.

The RESET pin of the display is unused and can be tied to VCC.

```
PC5:    SCK
PC6:    MOSI
PC0:    D/C
PC1:    CS
```

Building and flashing
=====================

The Makefile is configured to be run from a sub-subdirectory of the ch32v003fun repo (e.g.
 this directory can be installed under `examples/st7735`).
