#include <stdint.h>
#include "lcd_hw.h"

extern const uint8_t vgafont8[128*8];

int lcd_draw_char(int x, int y, uint16_t bg, uint16_t fg, char c) {
    // We have 128 characters.
    if (c > 127)
        c = 0;

    if (lcd_rect(x, y, 8, 8))
        return -1;

    if (lcd_ramwr())
        return -1;

    for (int j = 0; j < 8; j++) {
        uint8_t line = vgafont8[c * 8 + j];
        for (int i = 0; i < 8; i++) {
            lcd_write_raw(line & 0x80 ? fg : bg);
            line <<= 1;
        }
    }

    return 0;
}

int lcd_draw_string(int x, int y, uint16_t bg, uint16_t fg, char *s) {
    while (*s) {
        if (lcd_draw_char(x, y, bg, fg, *s))
            return -1;

        s++;
        x += 8;
    }

    return 0;
}
