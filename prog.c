#include <stdio.h>
#include "ch32v003fun.h"
#include "lcd_draw.h"

#define BG LCD_RGB565(8, 16, 8)
#define FG LCD_RGB565(16, 32, 31)

extern const uint16_t img_chart[];
int main() {
    SystemInit();
    printf("LCD HW init: %d\n", lcd_hw_init());

    lcd_display(1);

    lcd_fill_rect(0, 0, 160, 128, BG);
    lcd_draw_string(5, 5, BG, FG, "Hello, world!");

    lcd_draw_string(5, 15, BG, LCD_RGB565(31, 0, 0), "Red");
    lcd_draw_string(35, 15, BG, LCD_RGB565(0, 63, 0), "Grn");
    lcd_draw_string(65, 15, BG, LCD_RGB565(0, 0, 31), "Blu");
    lcd_draw_string(95, 15, BG, LCD_RGB565(31, 63, 0), "Yel");
    lcd_draw(img_chart, 56, 48, 48, 36);

    while(1)
        ;
}
